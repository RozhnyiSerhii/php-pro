<?php

use App\Url\Helpers\UrlValidator;
use App\Url\UrlShortener\Repositories\FileUrlCodeRepository;
use App\Url\UrlShortener\UrlShortener;

require_once __DIR__ . '/../vendor/autoload.php';

define('URL_SHORTENER_DIR', __DIR__ . '/../storage');
define('URL_SHORTENER_FILE', 'url-shortener.txt');
define('URL_SHORTENER_PATH', URL_SHORTENER_DIR . '/' . URL_SHORTENER_FILE);

$urls = [
    'https://www.freecodecamp.org/news/how-to-write-a-regular-expression-for-a-url/',
    'https://www.freecodecamp.org/news/how-to-write-a-regular-expression-for-a-url/',
    's://www.freecodecamp.org/news/how-to-write-a-regular-expression-for-a-url/',
    'https://lms.ithillel.ua/groups/6580b7c27ca8a3873fc6f32f/homeworks/65e84dcb24e20f3fcc3d4aa8',
    'http://lorem-ipsum.com',
    'https://gitlab.com/RozhnyiSerhii/php-pro',
    'https://gitlab.com/RozhnyiSerhii/php-proooooo',
    'https://refactoring.guru/uk/refactoring/smells',
    'https://swiperjs.com/kjhb',
    'swiperjs.com/kjhb'
];

$urlShortener = new UrlShortener(
    new FileUrlCodeRepository(URL_SHORTENER_PATH),
    new UrlValidator()
);

foreach ($urls as $url) {
    try {
        echo '-----------------------------' . PHP_EOL;
        echo $url . PHP_EOL;
        $code = $urlShortener->encode($url);
        echo '[CODE] ' .  $code . PHP_EOL;
        $decodedUrl = $urlShortener->decode($code);
        echo '[URL FROM CODE] ' . $decodedUrl . PHP_EOL;
    } catch (Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
}

try {
    echo '-----------------------------' . PHP_EOL;
    $unavailableCode = 'i1u23y45tyu44jh';
    echo '[UNAVAILABLE CODE] ' .  $unavailableCode . PHP_EOL;
    $decodedUrl = $urlShortener->decode($unavailableCode);
    echo '[URL FROM UNAVAILABLE CODE] ' . $decodedUrl . PHP_EOL;
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}