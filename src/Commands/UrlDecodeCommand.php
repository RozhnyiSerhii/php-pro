<?php

namespace App\Commands;

use App\Core\CLI\Commands\AbstractCommand;
use App\Url\Interfaces\IUrlDecoder;

class UrlDecodeCommand extends AbstractCommand
{
    const NAME = 'decode';

    public function __construct(protected IUrlDecoder $convertor)
    {
    }

    protected function runAction(): string
    {
        return 'Shortcode: ' . $this->convertor->decode($this->params[0]);
    }

    public static function getCommandDesc(): string
    {
        return 'Decode shortcode to url';
    }
}
