<?php

namespace App\FileSystem\Enums;

enum FileMode: string
{
    case READ = 'r';
    case READ_PLUS = 'r+';
    case WRITE = 'w';
    case WRITE_PLUS = 'w+';
    case APPEND = 'a';
    case APPEND_PLUS = 'a+';
}
