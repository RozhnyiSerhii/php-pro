<?php

namespace App\FileSystem;

use App\FileSystem\Enums\FileMode;
use InvalidArgumentException;

class File
{
    protected string $path;

    /**
     * @throws InvalidArgumentException
     */
    public function __construct(string $path)
    {
        $this->setPath($path);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setPath(string $path): void
    {
        self::createIfNotExist($path);
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function createIfNotExist(string $path): void
    {
        if (is_dir($path)) {
            throw new InvalidArgumentException('Invalid path. This is a directory.');
        }

        if (self::isFileExist($path)) {
            return;
        }

        $file = fopen($path, 'w+');
        fclose($file);
    }

    public static function isFileExist(string $path): bool
    {
        if (is_dir($path)) {
            return false;
        }

        if (file_exists($path)) {
            return true;
        }

        return false;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function getLineReaderGenerator(string $path): \Generator
    {
        self::checkFileExist($path);

        $file = fopen($path, 'r');
        while (($line = fgets($file)) !== false) {
            yield trim($line);
        }
        fclose($file);
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function append(string $path, string $content): void
    {
        self::write($path, $content, FileMode::APPEND_PLUS);

    }

    /**
     * @throws InvalidArgumentException
     */
    public static function rewrite(string $path, string $content): void
    {
        self::write($path, $content, FileMode::WRITE_PLUS);
    }

    /**
     * @throws InvalidArgumentException
     */
    protected static function write(string $path, string $content, FileMode $mode = FileMode::READ_PLUS): void
    {
        self::checkFileExist($path);

        $file = fopen($path, $mode->value);
        fwrite($file, $content);
        fclose($file);
    }

    /**
     * @throws InvalidArgumentException
     */
    protected static function checkFileExist(string $path): void
    {
        if (!self::isFileExist($path)) {
            throw new InvalidArgumentException('Invalid path. A file with this path does not exist.');
        }
    }
}
