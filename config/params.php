<?php
return [
    'db' => [
        'mysql' => [
            'type' => 'mysql',
            'host' => 'db_mysql',
            'port' => '13306',
            'dbname' => 'php_pro4',
            'user' => 'serhii_rozhnyi',
            'pass' => '1111',
        ],
    ],
    'devMode' => true,
    'monolog' => [
        'channel' => 'general',
        'level' => [
            'error' => __DIR__ . '/../var/logs/error.log',
            'info' => __DIR__ . '/../var/logs/info.log',
            'debug' => __DIR__ . '/../var/logs/debug.log',
            'notice' => __DIR__ . '/../var/logs/notice.log',
        ],
    ],
    'urlShortener' => [
        'length' => 3,
        'dbFile' => __DIR__ . '/../storage/url-shortener.txt',
    ],
];