<?php

use App\Commands\UrlDecodeCommand;
use App\Commands\UrlEncodeCommand;
use App\Core\DI\Enums\ServiceConfigArrayKeys as S;
use App\Url\UrlShortener\UrlShortener;

return [
    // ----------- COMMANDS -----------

    "cli.command.urlEncode" => [
        S::CLASSNAME => UrlEncodeCommand::class,
        S::ARGUMENTS => [
            '@' . UrlShortener::class
        ],
        S::TAGS => ['cli.command', 'allowed.command']
    ],
    "cli.command.urlDecode" => [
        S::CLASSNAME => UrlDecodeCommand::class,
        S::ARGUMENTS => [
            '@' . UrlShortener::class
        ],
        S::TAGS => ['cli.command', 'allowed.command']
    ],
];