<?php

use App\Core\DI\Container;
use App\Core\DI\Enums\ServiceConfigArrayKeys as S;
use App\Core\DI\ValueObjects\ServiceObject;
use App\ORM\ActiveRecord\DatabaseAR;
use App\Url\Helpers\UrlValidator;
use App\Url\UrlShortener\Repositories\DBUrlCodeRepository;
use App\Url\UrlShortener\Repositories\FileUrlCodeRepository;
use App\Url\UrlShortener\UrlShortener;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;

return [

    DatabaseAR::class => [
        S::CLASSNAME => DatabaseAR::class,
        S::ARGUMENTS => [
            '%db.mysql.dbname',
            '%db.mysql.user',
            '%db.mysql.pass',
            '%db.mysql.host',
        ]
    ],

    UrlShortener::class => [
        S::CLASSNAME => UrlShortener::class,
        S::ARGUMENTS => [
            '@' . DBUrlCodeRepository::class,
            '@' . UrlValidator::class,
            '%urlShortener.length',
        ],
    ],
    FileUrlCodeRepository::class => [
        S::CLASSNAME => FileUrlCodeRepository::class,
        S::ARGUMENTS => [
            '%urlShortener.dbFile',
        ],
    ],
    DBUrlCodeRepository::class => [
        S::CLASSNAME => DBUrlCodeRepository::class,
    ],
    UrlValidator::class => [
        S::CLASSNAME => UrlValidator::class,
    ],

    "monolog.logger" => [
        S::CLASSNAME => Logger::class,
        S::ARGUMENTS => [
            '%monolog.channel',
        ],
        S::COMPILER => function (Container $container, object $object, ServiceObject $refs) {
            /**
             * @var Logger $object
             */
            foreach ($container->getByTag('monolog.stream') as $item) {
                $object->pushHandler($item);
            }
        },
    ],
    "monolog.streamHandler.info" => [
        S::CLASSNAME => StreamHandler::class,
        S::ARGUMENTS => [
            '%monolog.level.info',
            Level::Info,
        ],
        S::TAGS => ['monolog.stream'],
    ],
    "monolog.streamHandler.error" => [
        S::CLASSNAME => StreamHandler::class,
        S::ARGUMENTS => [
            '%monolog.level.error',
            Level::Error,
        ],
        S::TAGS => ['monolog.stream'],
    ],
    "monolog.streamHandler.debug" => [
        S::CLASSNAME => StreamHandler::class,
        S::ARGUMENTS => [
            '%monolog.level.debug',
            Level::Debug,
        ],
        S::TAGS => ['monolog.stream'],
    ],
    "monolog.streamHandler.notice" => [
        S::CLASSNAME => StreamHandler::class,
        S::ARGUMENTS => [
            '%monolog.level.notice',
            Level::Notice,
        ],
        S::TAGS => ['monolog.stream'],
    ],

];